﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour 
{
	private static T _instance;

	private static object _lock = new object();
	private static bool _applicationIsQuitting = false;


	public static T Instance
	{
		get
		{
			if (_applicationIsQuitting) 
			{
				return null;
			}

			lock(_lock)
			{
				if (_instance == null) 
				{
					_instance = (T)FindObjectOfType (typeof(T));

					if (FindObjectsOfType (typeof(T)).Length > 1) 
					{
						//There is more than one object of this type on the scene
						//something is wrong

						return _instance;
					}

					if (_instance == null) 
					{
						//There is no object of this type in our scene, so we create one
						// and assign it to _instance property

						GameObject singleton = new GameObject ();
						_instance = singleton.AddComponent<T> ();
						singleton.name = "(S) " + typeof(T).ToString ();

						DontDestroyOnLoad (singleton);
					} 
					else
					{
						//We already have an object of this type in our scene so we will use it
					}
				}
				return _instance;
			}
		}

	}

	public void OnDestroy()
	{
		_applicationIsQuitting = true;
	}
}