﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

	public enum Type
	{
		Ammo = 0,
		Fuel = 1
	}

	public Collectible.Type type; 
	public int amount;
	private float timer = 10;
	private MeshRenderer[] renderers;

	// Use this for initialization
	void Start ()
	{
		renderers = GetComponentsInChildren<MeshRenderer> ();	
	}
	
	// Update is called once per frame
	void Update () 
	{
		timer -= Time.deltaTime;
		if (timer <= 0) 
		{
			Destroy (gameObject);	
		} 
		else if (timer < 1) 
		{
			for (int i = 0; i < renderers.Length; i++) 
			{
				renderers[i].enabled = Mathf.Sin (30 * Time.time) > 0;
			}
		}
		else if (timer < 3) 
		{
			for (int i = 0; i < renderers.Length; i++) 
			{
				renderers[i].enabled = Mathf.Sin (20 * Time.time) > 0;
			}
		}
		else if (timer < 6) 
		{
			for (int i = 0; i < renderers.Length; i++) 
			{
				renderers[i].enabled = Mathf.Sin (10 * Time.time) > 0;
			}
		}


	}
}
