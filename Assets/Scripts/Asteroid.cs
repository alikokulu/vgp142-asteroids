﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour 
{
	private bool ExplodeTriggered = false;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Explode()
	{
		if (ExplodeTriggered)
			return;

		ExplodeTriggered = true;
		if (transform.localScale == new Vector3(1,1,1)) 
		{
			//Spawn 2 0.5f size Asteroids	
			for (int i = 0; i < 2; i++) 
			{
				GameObject newAsteroid =  Instantiate (
					Resources.Load ("Asteroid"), 
					new Vector3(
						transform.position.x + Random.Range(0,1f), 
						transform.position.y + Random.Range(0,1f), 0),
					Quaternion.identity) as GameObject;	
				newAsteroid.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
				newAsteroid.GetComponent<Rigidbody> ().AddForce (
					new Vector3 (
						Random.Range (0, 100f), 
						Random.Range (0, 100f), 
						0));
			}
		} 
		else if(transform.localScale == new Vector3(0.5f,0.5f,0.5f))
		{
			//Spawn 3 0.25f size Asteroids
			for (int i = 0; i < 2; i++) 
			{
				GameObject newAsteroid =  Instantiate (
					Resources.Load ("Asteroid"), 
					new Vector3(
						transform.position.x + Random.Range(0,1f), 
						transform.position.y + Random.Range(0,1f), 0),
					Quaternion.identity) as GameObject;	
				newAsteroid.transform.localScale = new Vector3 (0.25f, 0.25f, 0.25f);
				newAsteroid.GetComponent<Rigidbody> ().AddForce (
					new Vector3 (
						Random.Range (0, 100f), 
						Random.Range (0, 100f), 
						0));
			}
		}

		//Drop bullet/fuel
		int rnd = Random.Range(0,100);
		if (rnd > 89) 
		{
			//Drop something
			if (rnd % 2 == 1)
			{
				//Ammo
				GameObject ammoObject = Instantiate(Resources.Load("Ammo"), transform.position,Quaternion.identity) as GameObject;
				Collectible c = ammoObject.GetComponent<Collectible> ();
				c.type = Collectible.Type.Ammo;
				c.amount = Random.Range (1, 10);
			}
			else 
			{
				//Fuel
				GameObject fuelObject = Instantiate(Resources.Load("Fuel"), transform.position,Quaternion.identity) as GameObject;
				Collectible c = fuelObject.GetComponent<Collectible> ();
				c.type = Collectible.Type.Fuel;
				c.amount = Random.Range (1, 10);
			}
		}


		//Particle Effects
		GameManager.Instance.Score++;
		Destroy (gameObject);
	}

	void OnTriggerEnter(Collider c)
	{
		if(c.transform.tag == "Player")
		{
			GameManager.Instance.PlayerFailure ();
		}
	}
}
