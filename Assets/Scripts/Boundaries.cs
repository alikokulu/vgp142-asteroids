﻿using UnityEngine;
using System.Collections;

public class Boundaries : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter(Collider c)
	{
		if (c.gameObject.tag != "Bullet") 
		{
			switch (gameObject.name) 
			{
				case "Top":
					c.transform.position = new Vector3 (c.transform.position.x, -10, 0);
					break;
				case "Bottom":
					c.transform.position = new Vector3 (c.transform.position.x, 10, 0);
					break;
				case "Left":
					c.transform.position = new Vector3 (21, c.transform.position.y, 0);
					break;
				case "Right":
					c.transform.position = new Vector3 (-21, c.transform.position.y, 0);
					break;
			}
		}
		else 
		{
			Destroy (c.gameObject);
		}




	}
}
