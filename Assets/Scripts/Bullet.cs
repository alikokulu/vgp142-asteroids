﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	Rigidbody rb;


	// Use this for initialization
	void Awake ()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Shoot(Vector3 force)
	{
		rb.AddForce (force);
		Destroy (this.gameObject, 1);
	}

	void OnTriggerEnter(Collider c)
	{
		if(c.tag == "Asteroid")
		{
			c.GetComponent<Asteroid> ().Explode ();
			Destroy (this.gameObject);
		}

	}
}
