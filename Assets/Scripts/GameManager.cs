﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager>
{
	protected GameManager(){}


	private GameObject asteroidPrefab; 
	private GameObject[] spawnPoints;
	public Vector3 shipPosition;
	public float minAsteroidSpeed = 5, maxAsteroidSpeed = 10;
	public int Score;
	public int WaveNumber = 1;
	private int lives = 3;

	//Store score
	//Spawning Asteroids
	//Player Lives

	// Use this for initialization
	void Start () 
	{
		asteroidPrefab = Resources.Load ("Asteroid") as GameObject;
		spawnPoints = GameObject.FindGameObjectsWithTag ("SpawnPoints");
		InvokeRepeating ("StartWave", 5, 30);
	}
	
	// Update is called once per frame
	void Update ()
	{
	}


	void SpawnAsteroid()
	{
		GameObject newAsteroid = 
			Instantiate (
				asteroidPrefab, 
				spawnPoints[Random.Range(0,spawnPoints.Length)].transform.position, 
				Quaternion.identity) as GameObject;

		newAsteroid.GetComponent<Rigidbody>().AddForce 
		((shipPosition - newAsteroid.transform.position) * 
			Random.Range(minAsteroidSpeed, maxAsteroidSpeed));
	}


	void StartWave()
	{
		for (int i = 0; i < WaveNumber; i++) 
		{
			SpawnAsteroid ();	
		}

		WaveNumber++;
	}

	public void PlayerFailure()
	{
		lives--;
		Debug.Log ("Remaining Lives: " + lives);
		if (lives <= 0 ) 
		{
			//Game Over
			Time.timeScale = 0;
			//Next Class, show GUI with Score
		}
	}
}
