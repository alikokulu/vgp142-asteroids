﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour
{

	private Rigidbody rigidBody;
	private GameObject bullet;

	public float ThrustForce = 500;
	public float RotationForce = 100;
	public float BulletForce = 1000;

	public float shootInterval = 0.5f;
	private float shootWaitTime = 0;

	public float Fuel = 100;
	public float Ammo = 100;




	public Transform[] weapons;


	// Use this for initialization
	void Start ()
	{
		rigidBody = GetComponent<Rigidbody> ();
		rigidBody.maxAngularVelocity = 3;
		bullet = Resources.Load ("Bullet") as GameObject;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKey(KeyCode.Escape))
		{
			rigidBody.velocity = Vector3.zero;
			rigidBody.angularVelocity = Vector3.zero;
		}


		if (Fuel > 0) {
			if (Input.GetKey (KeyCode.UpArrow)) {
				//Forward Thrust
				rigidBody.AddForce (transform.up * ThrustForce * Time.deltaTime);
				Fuel -= Time.deltaTime * 2;
			} else if (Input.GetKey (KeyCode.DownArrow)) {
				//Backward Thrust
				rigidBody.AddForce (-transform.up * ThrustForce * Time.deltaTime);
				Fuel -= Time.deltaTime * 2;
			}


			if (Input.GetKey (KeyCode.RightArrow)) {
				//Rotate Right
				rigidBody.AddTorque (new Vector3 (0, 0, -RotationForce * Time.deltaTime));
				Fuel -= Time.deltaTime * 2;

			} else if (Input.GetKey (KeyCode.LeftArrow)) {
				//Rotate Left	
				rigidBody.AddTorque (new Vector3 (0, 0, RotationForce * Time.deltaTime));
				Fuel -= Time.deltaTime * 2;
			}
		} else {
			Fuel = 0;
		}


		if (Input.GetKey (KeyCode.Space)) {
			Shoot ();
		}

		if (Input.GetKeyUp (KeyCode.Space)) {
			shootWaitTime = 0;
		}

		if (rigidBody.velocity.magnitude > 5) {
			rigidBody.velocity = rigidBody.velocity * (5 / rigidBody.velocity.magnitude);
		}

		GameManager.Instance.shipPosition = transform.position;
	}

	void Shoot ()
	{
		if (Ammo <= 0)
			return;

		if (shootWaitTime <= 0) {
			//Create & Shoot Bullets
			for (int i = 0; i < weapons.Length; i++) {
				GameObject currentBullet = Instantiate (bullet, weapons [i].position, Quaternion.identity) as GameObject;
				currentBullet.GetComponent<Bullet> ().Shoot (transform.up * BulletForce);
			}

			Ammo -= 2;

			//Reset Wait Time to interval
			shootWaitTime = shootInterval;
		} else {
			shootWaitTime -= Time.deltaTime;
		}

	}

	void OnTriggerEnter (Collider c)
	{
		if (c.gameObject.tag == "Collectible")
		{
			Collectible coll = c.GetComponent<Collectible> ();
			Debug.Log (coll.type);
			switch (coll.type) 
			{
				case Collectible.Type.Ammo:
					Ammo += coll.amount;
					if (Ammo > 100)
						Ammo = 100;
					break;

				case Collectible.Type.Fuel:
					Fuel += coll.amount;
					if (Fuel > 100)
						Fuel = 100;
					break;
			}
			Destroy (c.gameObject);
		}

	}

}
